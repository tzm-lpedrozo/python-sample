from flask import jsonify, Flask
from flask_cors import CORS

app = Flask(__name__)
app.config["DEBUG"] = True
CORS(app)

pessoas = [
    {"id": 1, "nome": "Pedro de Lara"},
    {"id": 2, "nome": "Sílvio Santos"},
    {"id": 3, "nome": "Décio Piccinini"},
    {"id": 4, "nome": "Wagner Montes"},
    {"id": 5, "nome": "Sônia Lima"},
    {"id": 6, "nome": "Sérgio Mallandro"}
]


@app.route("/")
def home():
    return "Home"


@app.route("/pessoas/")
def find_all():
    return jsonify(pessoas)


@app.route("/pessoas/<int:pid>")
def find_by_id(pid):
    return jsonify(list(filter(lambda i: i["id"] == pid, pessoas)))


app.run()
