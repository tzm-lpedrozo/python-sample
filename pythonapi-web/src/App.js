import React, {Component} from "react";
import "./App.css";
import {Column} from "primereact/column";
import {DataTable} from "primereact/datatable";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";

export default class App extends Component {

	state = {
		pessoas: []
	};

	componentDidMount() {
		fetch("http://localhost:5000/pessoas/").then((response) => {
			if (response.status === 200) {
				response.json().then((pessoas) => this.setState({pessoas}));
			}
		})
	}

	render() {
		return (
			<div className="p-grid">
				<div className="p-col-6">
					<DataTable header="Pessoas" value={this.state.pessoas}>
						<Column header="ID" field="id"/>
						<Column header="Nome" field="nome"/>
					</DataTable>
				</div>
			</div>
		);
	}

}
